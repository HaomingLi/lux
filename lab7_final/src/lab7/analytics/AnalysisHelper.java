/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lab7.entities.Comment;
import lab7.entities.User;
import java.util.Arrays;
import java.util.Map.Entry;
import lab7.entities.Post;
/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    // find user with Most Likes
    // TODO
    public void userWithMostLikes(){
        Map<Integer,Integer> userlikesCount = new HashMap<>();
        Map<Integer,User> users = DataStore.getInstance().getUsers();   
        
        for(User user:users.values()){
            for(Comment c:user.getComments()){
                int likes = 0;
                if(userlikesCount.containsKey(user.getId()))
                    likes = userlikesCount.get(user.getId());
                likes+=c.getLikes();
                userlikesCount.put(user.getId(), likes);
                
            }
        }
        int max = 0;
        int maxId = 0;
        
        for(int id:userlikesCount.keySet()){
            if(userlikesCount.get(id)>max){
                max = userlikesCount.get(id);
                maxId = id;
            }
        }
        
        System.out.println("User with most likes: "+ max +"\n"+users.get(maxId));
    }
   
    public void getFiveMostLikedComment(){
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentlist = new ArrayList<>(comments.values());
         
        Collections.sort(commentlist, new Comparator<Comment>(){
            public int compare(Comment c1, Comment c2){
                return c2.getLikes()- c1.getLikes();
            }
        });
        System.out.println("5 most likes Comments: ");
        for(int i=0;i<commentlist.size()&&i<5;i++){
                System.out.println(commentlist.get(i)); 
        }
    }
    
public void getAverageLikesComment(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();       
        List<Comment> commentlist = new ArrayList<>(comments.values());
        
        int likesSum=0;
        for(int i=0;i<commentlist.size();i++){
            likesSum+=commentlist.get(i).getLikes();
        }
        int averageLikes=likesSum/commentlist.size();
        System.out.println("Average number of likes per comment: "+averageLikes);
    }

       public void getPostWithMostLikesComment(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentlist = new ArrayList<>(comments.values());
         
        Collections.sort(commentlist, new Comparator<Comment>(){
            public int compare(Comment c1, Comment c2){
                return c2.getLikes()- c1.getLikes();
            }
        });
        
        for(Post post:posts.values()){
            if(commentlist.get(0).getPostId()==post.getPostId()){
                System.out.println("the post with most liked comments: No."+post.getPostId());
                return;
            }
        }
    }
       
       public void getPostWithMostComment(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
        List<Comment> commentlist = new ArrayList<>(comments.values());

        int maxComment=0;
        for(Post post:posts.values()){
            if(post.getComments().size()>maxComment){
                maxComment=post.getComments().size();
            }
        }
        
        for(Post post:posts.values()){
            if(post.getComments().size()==maxComment){
                System.out.println("the post with most comments: No."+post.getPostId());
            }
        }
    }
       
       public void Top5InactiveUsersOnTotalPostsNumber(){       
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer,Integer> userPostCount = new HashMap<>();
               
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<>(posts.values());
     
        
        for(User user:users.values()){
            int count = 0;
            for(Post post:postList){
                if(post.getUserId()==user.getId()){             
                    count++;
                    userPostCount.put(user.getId(), count);              
                }
            }         
        }
        List<Map.Entry<Integer,Integer>> userPostCountList = new ArrayList<>(userPostCount.entrySet());
        Collections.sort(userPostCountList, new Comparator<Map.Entry<Integer,Integer>>(){
            @Override
            public int compare(Entry<Integer,Integer> c1, Entry<Integer,Integer> c2){
                return c1.getValue() - c2.getValue();
            }
        });
        
        System.out.println("Top 5 inactive users based on total posts number: ");
        for(int i=0;i<userPostCountList.size()&&i<5;i++){
                System.out.println("User: No."+userPostCountList.get(i).getKey()+" Post Number: "+userPostCountList.get(i).getValue()); 
        }     
    }
       
        public void getFiveMostInactiveUsersOnCommentNumberMethod1(){       
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();

        Map<Integer,Integer> userCommentCount = new HashMap<>();
        List<Comment> commentList = new ArrayList<>(comments.values());
        
        for(User user:users.values()){
            int count = 0;
            for(Comment comment:commentList){
                if(comment.getUserId()==user.getId()){             
                    count++;
                    userCommentCount.put(user.getId(), count);              
                }
            }         
        }
        List<Map.Entry<Integer,Integer>> userCommentCountList = new ArrayList<>(userCommentCount.entrySet());
        Collections.sort(userCommentCountList, new Comparator<Map.Entry<Integer,Integer>>(){
            @Override
            public int compare(Entry<Integer,Integer> c1, Entry<Integer,Integer> c2){
                return c1.getValue() - c2.getValue();
            }
        });
        
        System.out.println("Top 5 inactive users based on total comments number: ");
        for(int i=0;i<userCommentCountList.size()&&i<5;i++){
                System.out.println("User: No."+userCommentCountList.get(i).getKey()+" Comments Number: "+userCommentCountList.get(i).getValue()); 
        }     
    }
       
       
       
       
       
    
//    public void getFiveMostInactiveUsersOnCommentNumberMethod2()
//    {
//        Map<Integer, Comment> comments= DataStore.getInstance().getComments();
//        List<Comment> commentlist = new ArrayList<>(comments.values());
//        ArrayList<String> list1 = new ArrayList<String>();
//        for(int i=0; i<commentlist.size();i++)
//        {
//            list1.add(String .valueOf(commentlist.get(i).getUserId()));
//        }
//        int [][] list2 = new int[10][2];
//        int i,j;
//        Set uniqueSet = new HashSet(list1);
//        System.out.println("Comment Count Per User:");
//        for (Object temp : uniqueSet) {
//		System.out.println("User"+temp + ": " + Collections.frequency(list1, temp));   
//                i=Integer.parseInt(String.valueOf(temp));
//                list2 [i][0]= Collections.frequency(list1, temp);
//                list2 [i][1]= i;
//	}
//        Arrays.sort(list2, new Comparator<int[]>(){
//        public int compare(int[] o1, int[] o2) {
//        return o1[0] - o2[0];
//        }
//        });  
//        System.out.println("Top 5 inactive users based on total comments number(method 2):");
//        for(int m=0; m<5; m++)
//        { System.out.println("Comment Count:"+list2[m][0]+" UserId:"+list2[m][1]);}
//    }
        
    
   public void getFiveMostInactiveUsersOnOverall()
   {
       Map<Integer, User> users = DataStore.getInstance().getUsers();     //user
       Map<Integer, Comment> comments= DataStore.getInstance().getComments();    //comment
       Map<Integer, Post> posts = DataStore.getInstance().getPosts();     //post

       Map<Integer,Integer> userSumCount = new HashMap<>();
       
       List<Comment> commentList = new ArrayList<>(comments.values());
       List<Post> postList = new ArrayList<>(posts.values());
     
        
        for(User user:users.values()){
            int count1 = 0;
            for(Post post:postList){
                if(post.getUserId()==user.getId()){             
                    count1 ++;
                    //userPostCount.put(user.getId(), count1);              
                }
            }   
            int count2 = 0;
            int count3=0;  //likes
            for(Comment comment:commentList){
                if(comment.getUserId()==user.getId()){             
                    count2 ++;
                    count3 = count3 + comment.getLikes();
                    //userCommentCount.put(user.getId(), count2);              
                }
            }        
            int sum =0;
                    sum = count1 + count2 +count3;
                    userSumCount.put(user.getId(), sum);
        }
        
        List<Map.Entry<Integer,Integer>> userSumList = new ArrayList<>(userSumCount.entrySet());
        Collections.sort(userSumList, new Comparator<Map.Entry<Integer,Integer>>(){
            @Override
            public int compare(Entry<Integer,Integer> c1, Entry<Integer,Integer> c2){
                return c1.getValue() - c2.getValue();
            }
        });
        
        System.out.println("Top 5 inactive users based on overall: ");
        for(int i=0;i<userSumList.size()&&i<5;i++){
                System.out.println("User: No."+userSumList.get(i).getKey()+" Overall : "+userSumList.get(i).getValue()); 
        }   
        
   }
   
   public void getFiveMostProactiveUsersOnOverall()
   {
       Map<Integer, User> users = DataStore.getInstance().getUsers();     //user
       Map<Integer, Comment> comments= DataStore.getInstance().getComments();    //comment
       Map<Integer, Post> posts = DataStore.getInstance().getPosts();     //post
   
       Map<Integer,Integer> userSumCount = new HashMap<>();
       
       List<Comment> commentList = new ArrayList<>(comments.values());
       List<Post> postList = new ArrayList<>(posts.values());

        for(User user:users.values()){
            int count1 = 0;
            for(Post post:postList){
                if(post.getUserId()==user.getId()){             
                    count1 ++;
                                
                }
            }   
            int count2 = 0;
            int count3=0;  //likes
            for(Comment comment:commentList){
                if(comment.getUserId()==user.getId()){             
                    count2 ++;
                    count3 = count3 + comment.getLikes();
                                
                }
            }        
            int sum =0;
                    sum = count1 + count2 +count3;
                    userSumCount.put(user.getId(), sum);
        }
        List<Map.Entry<Integer,Integer>> userSumList = new ArrayList<>(userSumCount.entrySet());
        Collections.sort(userSumList, new Comparator<Map.Entry<Integer,Integer>>(){
            @Override
            public int compare(Entry<Integer,Integer> c1, Entry<Integer,Integer> c2){
                return c2.getValue() - c1.getValue();
            }
        });

        System.out.println("Top 5 Proactive users based on overall: ");
        for(int i=0;i<userSumList.size()&&i<5;i++){
                System.out.println("User: No."+userSumList.get(i).getKey()+" Overall: "+userSumList.get(i).getValue()); 
        }   
        
   }
   
}
