package viewinterface.airliner;


import viewinterface.agency.ReserveJpanel;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.util.Map;
import javax.swing.table.DefaultTableModel;


import business.Flight;

public class FlightSearchJpanel extends javax.swing.JPanel {

    private ArrayList<Flight> flightList;
    private JPanel funcJpanel;
    private Map customerMap;

    public FlightSearchJpanel(JPanel funcJpanel, ArrayList flightList, Map customerMap) {
        initComponents();
        this.funcJpanel = funcJpanel;
        this.flightList = flightList;
        this.customerMap = customerMap;
        flushTable();
    }

    private void flushTable() {
        DefaultTableModel dTable = (DefaultTableModel) flightTable.getModel();
        dTable.setRowCount(0);

        for (Flight flight : this.flightList) {
            Object row[] = new Object[8];
            row[0] = flight;
            row[1] = flight.getDeparture();
            row[2] = flight.getArrival();
            row[3] = flight.getDepartureTime();
            row[4] = flight.getArrivalTime();
            row[5] = getLastTimeString(flight.getLastTime()/1000);
            dTable.addRow(row);
        }
    }
    
    private String getLastTimeString(int totalTime){
        String totalTimeStr = totalTime / 86400 + "d" + (totalTime / 3600) % 24 + "h" + (totalTime % 3600) / 60 + "min" + (totalTime % 3600) % 60 + "s";
        return totalTimeStr;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        reserveBtn = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        flightTable = new javax.swing.JTable();
        titleJlael = new javax.swing.JLabel();
        viewBtn = new javax.swing.JButton();
        backBtn = new javax.swing.JButton();

        setBackground(new java.awt.Color(102, 204, 255));

        reserveBtn.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        reserveBtn.setText("Reserve");
        reserveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reserveBtnActionPerformed(evt);
            }
        });

        flightTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "FlightID", "Departure", "Arrival", "DepartureTime", "ArrivalTime", "Last"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        flightTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(flightTable);

        titleJlael.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        titleJlael.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleJlael.setText("Flight Information");

        viewBtn.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        viewBtn.setText("View");
        viewBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewBtnActionPerformed(evt);
            }
        });

        backBtn.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        backBtn.setText("Back");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 190, Short.MAX_VALUE)
                        .addComponent(viewBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(reserveBtn)
                        .addGap(32, 32, 32))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(titleJlael, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(141, 141, 141))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(titleJlael, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(viewBtn)
                    .addComponent(backBtn)
                    .addComponent(reserveBtn))
                .addGap(30, 30, 30))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void reserveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reserveBtnActionPerformed
        int selectedRow = flightTable.getSelectedRow(), col = 0;
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Please select at least a row.", "INFORMATION", JOptionPane.ERROR_MESSAGE);
            return;
        }

        Flight flight = (Flight) flightTable.getValueAt(selectedRow, col);
        this.funcJpanel.add(new ReserveJpanel(this.funcJpanel, flight, this.customerMap));
        CardLayout cLayout = (CardLayout) this.funcJpanel.getLayout();
        cLayout.next(funcJpanel);
    }//GEN-LAST:event_reserveBtnActionPerformed

    private void viewBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewBtnActionPerformed
        int selectedRow = flightTable.getSelectedRow(), col = 0;
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Please select at least a row.", "INFORMATION", JOptionPane.ERROR_MESSAGE);
            return;
        }

        Flight flight = (Flight) flightTable.getValueAt(selectedRow, col);
        this.funcJpanel.add(new FlightJpanel(this.funcJpanel, flight));
        CardLayout cLayout = (CardLayout) this.funcJpanel.getLayout();
        cLayout.next(funcJpanel);
    }//GEN-LAST:event_viewBtnActionPerformed

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        funcJpanel.remove(this);
        CardLayout cLayout = (CardLayout) funcJpanel.getLayout();
        cLayout.previous(funcJpanel);
    }//GEN-LAST:event_backBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backBtn;
    private javax.swing.JTable flightTable;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton reserveBtn;
    private javax.swing.JLabel titleJlael;
    private javax.swing.JButton viewBtn;
    // End of variables declaration//GEN-END:variables
}
