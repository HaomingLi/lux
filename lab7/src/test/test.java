package test;

import java.io.IOException;

import test.analytics.AnalysisHelper;
import test.entities.Comment;

/**
 *
 * @author lhm
 */
public class test {
    DataReader commentReader;
    DataReader userReader;
    AnalysisHelper helper;
    
    public test() throws IOException {
        DataGenerator generator = DataGenerator.getInstance();
        commentReader = new DataReader(generator.getCommentFilePath());
        userReader = new DataReader(generator.getUserCataloguePath());
        helper = new AnalysisHelper();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {     
        test inst = new test();
        inst.readData();
    }
    
    private void readData() throws IOException{
        String[] row;
        while((row = userReader.getNextRow()) != null ){
            generateUser(row);
        }
        while((row = commentReader.getNextRow()) != null ){
            Comment comment = generateComment(row);
            generatePost(row, comment);
        }
        runAnalysis();
    }
    
    private void generateUser(String[] row){
        // TODO
    }
    
    private Comment generateComment(String[] row){
//        Map User = new xxx;
//        if (user.haskey(commentingUser))
//            map.getuser.getcomment.add(comment)
     
        return null;
    }
    
    private void generatePost(String[] row, Comment comment){
//        if post.haskey(post)
//            post.get(postid).getcomment.add
//        else
//            new post
    }
    
    private void runAnalysis(){
        // TODO
    }
}
