/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.HashMap;
import java.util.Map;


public class SalesPersonDirectory {
    private Map<Integer, SalesPerson> salesPersonList;
    
    public SalesPersonDirectory(){
         salesPersonList = new HashMap<>();
    }

    public Map<Integer, SalesPerson> getSalesPersonList() {
        return salesPersonList;
    }

    public void setSalesPersonList(Map<Integer, SalesPerson> salesPersonList) {
        this.salesPersonList = salesPersonList;
    }
    
    public void addSalesPerson(SalesPerson s){
        if (salesPersonList.containsKey(s.getSalesPersonId())) {
            return;
        }
        salesPersonList.put(s.getSalesPersonId(),s );
    }
}
