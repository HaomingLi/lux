/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;


public class Product {
    int productId;
    int minPrice;
    int maxPrice;
    int tarPrice;

    public Product(int productId, int minPrice, int maxPrice, int tarPrice){
        this.productId = productId;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.tarPrice = tarPrice;
               
    }
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public int getTarPrice() {
        return tarPrice;
    }

    public void setTarPrice(int tarPrice) {
        this.tarPrice = tarPrice;
    }
    
    
}
