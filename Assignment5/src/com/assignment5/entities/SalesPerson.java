/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;


public class SalesPerson {
    private int salesPersonId;
    private List<Order> orderList = new ArrayList<>();
    
//    public SalesPerson(){
//         orderList = new ArrayList<>();
//     }         
     
     public SalesPerson(int x){
         this.salesPersonId = x;
         orderList = new ArrayList<>();
    }
    
    public int getSalesPersonId() {
        return salesPersonId;
    }

    public void setSalesPersonId(int salesPersonId) {
        this.salesPersonId = salesPersonId;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }
    
    @Override
    public String toString() {
        return "Salesperson{" + "id = " + salesPersonId + ", no. of orders = " + orderList.size() + '}';
    }
}
