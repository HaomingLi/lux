/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;


public class Customer {
      private int CustomerId;
      private  List<Order> orderList;
              
//     public Customer(){
//         orderList = new ArrayList<>();
//     }         
     
     public Customer (int x){
         this.CustomerId = x;
         orderList = new ArrayList<>();
    }
     
    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int CustomerId) {
        this.CustomerId = CustomerId;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }
      
    @Override
    public String toString() {
        return "Customer{" + "id = " + CustomerId + ", no. of orders = " + orderList.size() + '}';
    }
      
}
