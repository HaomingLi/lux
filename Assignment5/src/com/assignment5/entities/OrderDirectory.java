/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.HashMap;
import java.util.Map;


public class OrderDirectory {
     private Map<Integer, Order> orDir;
     
     public OrderDirectory(){
         orDir = new HashMap<>(); 
     }

    public Map<Integer, Order> getOrderDir() {
        return orDir;
    }

    public void setOrDir(Map<Integer, Order> orDir) {
        this.orDir = orDir;
    }
     
    public void addOrder(Order o){
         if(orDir.containsKey(o.orderId)){
              return;
         }
         
         orDir.put(o.orderId, o);
    } 
}
