/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.analytics;

import com.assignment5.entities.CustomerDirectory;
import com.assignment5.entities.OrderDirectory;
import com.assignment5.entities.ProductCatalog;
import com.assignment5.entities.SalesPersonDirectory;


public class DataStore {
     private static DataStore dataStore;
     
     private CustomerDirectory custDir;
     private OrderDirectory orDir;
     private ProductCatalog proDir;
     private SalesPersonDirectory salesDir;
     
     private DataStore(){
         custDir = new CustomerDirectory();
         orDir = new OrderDirectory();
         proDir = new ProductCatalog();
         salesDir = new SalesPersonDirectory();
     }
     
     public static DataStore getInstance(){
        if(dataStore == null)
            dataStore = new DataStore();
        return dataStore;
    }
     
     public static DataStore getDataStore() {
        return dataStore;
    }
    public static DataStore getNewDataStore(){
        dataStore = new DataStore();
        return dataStore;
    }
    public static void setDataStore(DataStore dataStore) {
        DataStore.dataStore = dataStore;
    }

    public CustomerDirectory getCustDir() {
        return custDir;
    }

    public void setCustDir(CustomerDirectory custDir) {
        this.custDir = custDir;
    }

    public OrderDirectory getOrDir() {
        return orDir;
    }

    public void setOrDir(OrderDirectory orDir) {
        this.orDir = orDir;
    }

    public ProductCatalog getProDir() {
        return proDir;
    }

    public void setProDir(ProductCatalog proDir) {
        this.proDir = proDir;
    }

    public SalesPersonDirectory getSalesDir() {
        return salesDir;
    }

    public void setSalesDir(SalesPersonDirectory salesDir) {
        this.salesDir = salesDir;
    }
    
    
     
}
