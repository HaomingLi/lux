/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.analytics;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnalysisHelper {
      //top 3 best negotiated products
    public void getTopThreeBestNegotiatedProd(){
        Map<Integer, Order> orders = DataStore.getInstance().getOrDir().getOrderDir();
        Map<Integer, Product> products = DataStore.getInstance().getProDir().getProductDir();
        List<Order> orderList = new ArrayList<Order>(orders.values());
        //List<Product> productList = new ArrayList<Product>(products.values());
        Map<Integer, Integer> bnProd = new HashMap<>();
        for(int i = 0; i < orderList.size(); i++){
            Item item = orders.get(i).getItem();
            int id = item.getProductId();
             if(products.containsKey(id)){
                  if(products.get(id).getTarPrice() < item.getSalesPrice()){
                       if(bnProd.containsKey(id)){
                             int q = bnProd.get(id);
                             int aq = item.getQuantity();
                             bnProd.put(id, q+aq);
                       }else{
                           bnProd.put(id, item.getQuantity());
                       }
                  }
              }
        }
        
         List<Map.Entry<Integer, Integer>> list = new ArrayList<Map.Entry<Integer, Integer>>(bnProd.entrySet());
         Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
           @Override
           public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
               return o2.getValue().compareTo(o1.getValue());
           }
       });
       int n = 1;
       int temp = list.get(0).getValue();
       System.out.println("1)top 3 best negotiated products");
       System.out.println("ProductId : Quantity");
       for(int i = 0; i < list.size() && n < 4; i++){
           
           
                if(list.get(i).getValue() != temp){
                     temp = list.get(i).getValue();
                     n = n + 1;

                }
                if(n < 4){
                    
                    System.out.println(list.get(i).getKey() +"      : " + list.get(i).getValue());
                }
           }
    }
    
    public void getThreeBestCustomers(){
         Map<Integer, Customer> customers = DataStore.getInstance().getCustDir().getCustomerDir();
         List<Customer> customerList = new ArrayList<Customer>(customers.values());
         Map<Integer, Product> products = DataStore.getInstance().getProDir().getProductDir();
         Map<Customer, Integer> customerSum = new HashMap<>();
         for(int i = 0; i < customerList.size(); i++){
             Customer c = customerList.get(i);
             int s = 0;
             for(Order o : c.getOrderList()) {
                  int salesPrice = o.getItem().getSalesPrice();
                  int tarPrice = products.get(o.getItem().getProductId()).getTarPrice();
                  int sum = (Math.abs(salesPrice - tarPrice))* o.getItem().getQuantity();
                  s = s + sum;
             }
             customerSum.put(customerList.get(i), s);
         }
         
         List<Map.Entry<Customer, Integer>> list = new ArrayList<Map.Entry<Customer, Integer>>(customerSum.entrySet());
         Collections.sort(list, new Comparator<Map.Entry<Customer, Integer>>() {
           @Override
           public int compare(Map.Entry<Customer, Integer> o1, Map.Entry<Customer, Integer> o2) {
               return o1.getValue().compareTo(o2.getValue());
           }
       });
       int n = 1;
       int temp = list.get(0).getValue();
       System.out.println("2)top 3 best Customers");
       System.out.println("CustomerId : Sum");
       for(int i = 0; i < list.size() && n < 4; i++){
           
           
                if(list.get(i).getValue() != temp){
                     temp = list.get(i).getValue();
                     n = n + 1;

                }
                if(n < 4){
                    
                    System.out.println(list.get(i).getKey().getCustomerId() +"      : " + list.get(i).getValue());
                }
           }
         
         
    }
    
    /*public void getThreeBestCustomers(){
         Map<Integer, Customer> customers = DataStore.getInstance().getCustDir().getCustomerDir();
         List<Customer> customerList = new ArrayList<Customer>(customers.values());
         Map<Integer, Product> products = DataStore.getInstance().getProDir().getProductDir();
         Map<Customer, Double> customerSum = new HashMap<>();
         for(int i = 0; i < customerList.size(); i++){
             Customer c = customerList.get(i);
             int s = 0;
             int q = 0;
             for(Order o : c.getOrderList()) {
                  int salesPrice = o.getItem().getSalesPrice();
                  int tarPrice = products.get(o.getItem().getProductId()).getTarPrice();
                  int sum = Math.abs(salesPrice - tarPrice) * o.getItem().getQuantity();
                  s = s + sum;
                  q = q + o.getItem().getQuantity();
             }
             double a = s/q;
             customerSum.put(customerList.get(i), a);
         }
         
         List<Map.Entry<Customer, Double>> list = new ArrayList<Map.Entry<Customer, Double>>(customerSum.entrySet());
         Collections.sort(list, new Comparator<Map.Entry<Customer, Double>>() {
           @Override
           public int compare(Map.Entry<Customer, Double> o1, Map.Entry<Customer, Double> o2) {
               return o1.getValue().compareTo(o2.getValue());
           }
       });
       int n = 1;
       double temp = list.get(0).getValue();
       System.out.println("2)top 3 best Customers");
       System.out.println("CustomerId : Sum");
       for(int i = 0; i < list.size() && n < 4; i++){
           
           
                if(list.get(i).getValue() != temp){
                     temp = list.get(i).getValue();
                     n = n + 1;

                }
                if(n < 4){
                    
                    System.out.println(list.get(i).getKey().getCustomerId() +"      : " + list.get(i).getValue());
                }
           }
         
         
    }*/
    
    public void getThreeBestSalesPerson(){
        Map<Integer, SalesPerson> salespersons = DataStore.getInstance().getSalesDir().getSalesPersonList();
        List<SalesPerson> sList = new ArrayList<>(salespersons.values());
        Map<Integer, Product> products = DataStore.getInstance().getProDir().getProductDir();
        Map<SalesPerson, Integer> salesProfit = new HashMap<>();
        for(int i = 0; i < sList.size(); i++){
             SalesPerson s = sList.get(i);
             int p = 0;
             for(Order o : s.getOrderList()){
                 int salesPrice = o.getItem().getSalesPrice();
                 int tarPrice = products.get(o.getItem().getProductId()).getTarPrice();
                 int quantity = o.getItem().getQuantity();
                 int profit = (salesPrice - tarPrice) * quantity;
                 p = p + profit;
             }
             salesProfit.put(s, p);
        }
        
        List<Map.Entry<SalesPerson, Integer>> list = new ArrayList<Map.Entry<SalesPerson, Integer>>(salesProfit.entrySet());
         Collections.sort(list, new Comparator<Map.Entry<SalesPerson, Integer>>() {
           @Override
           public int compare(Map.Entry<SalesPerson, Integer> o1, Map.Entry<SalesPerson, Integer> o2) {
               return o2.getValue().compareTo(o1.getValue());
           }
       });
       int n = 1;
       int temp = list.get(0).getValue();
       System.out.println("3)top 3 best Salespersons");
       System.out.println("SalesPersonId : Profit");
       for(int i = 0; i < list.size() && n < 4; i++){
                if(list.get(i).getValue() != temp){
                     temp = list.get(i).getValue();
                     n = n + 1;

                }
                if(n < 4){
                    
                    System.out.println(list.get(i).getKey().getSalesPersonId() +"      : " + list.get(i).getValue());
                }
           }
    }
    
    public void getTotalRevenue(){
        Map<Integer, SalesPerson> salespersons = DataStore.getInstance().getSalesDir().getSalesPersonList();
        List<SalesPerson> sList = new ArrayList<>(salespersons.values());
        Map<Integer, Product> products = DataStore.getInstance().getProDir().getProductDir();
        int totalP = 0;
        for(int i = 0; i < sList.size(); i++){
             SalesPerson s = sList.get(i);
             int p = 0;
             for(Order o : s.getOrderList()){
                 int salesPrice = o.getItem().getSalesPrice();
                 int tarPrice = products.get(o.getItem().getProductId()).getTarPrice();
                 int quantity = o.getItem().getQuantity();
                 int profit = (salesPrice - tarPrice) * quantity;
                 p = p + profit;
             }
             totalP = totalP + p;
        }
        
        System.out.println("4)Total revenue: " + totalP);
        
    }
    
    public void getAveragePrice(){
    Map<Integer,Product> product = DataStore.getInstance().getProDir().getProductDir();
    List<Product> productList = new ArrayList<Product>(product.values());
   
    Map<Integer,Order> order = DataStore.getInstance().getOrDir().getOrderDir();
    List<Order> orderList = new ArrayList<Order>(order.values());
    
    Map<Integer, Double> averagePrice = new HashMap<>();
    Map<Integer, Integer> tarPrice = new HashMap<>();
    Map<Integer, Double> diff = new HashMap<>();
    Map<Integer, String> listed = new HashMap<>();
    System.out.println("5.1)Original Data--------------------------------------");
    for(int i =0; i<product.size(); i++) {
         Product tmpProduct = product.get(i);
         int revenue = 0;
         int quantity = 0;
         for(int j =0;j< orderList.size();j++){
            if(order.get(j).getItem().getProductId() == tmpProduct.getProductId()){
               revenue += order.get(j).getItem().getSalesPrice()* order.get(j).getItem().getQuantity();
               quantity +=order.get(j).getItem().getQuantity(); 
            }  
        }
         averagePrice.put(i,(double)revenue/quantity);
         tarPrice.put(i,tmpProduct.getTarPrice());
         diff.put(i,(tarPrice.get(i) - averagePrice.get(i)));
         listed.put(i, "No");
       }
       System.out.println("Table1:average sale price lower than the target price ");
       int t=0;
       for(int i=0; i<product.size(); i++){
           if(listed.get(i)=="No"){
           int num=i;
           for(int n=i+1;n<product.size();n++){
         if(diff.get(num)<diff.get(n) && (listed.get(n)=="No"))
         num=n;
           }
           if(diff.get(num)<0 && t==0){
            System.out.println("Table2:average sale price higher than the target price "); 
            t=1;
           }
         System.out.println("Product ID: " + num + "| Average Price: " + averagePrice.get(num) + " | Target Price: " +tarPrice.get(num) + " | Difference: " + (diff.get(num)));
           listed.put(num,"Yes");
       }
     }
    }
       
    
    public void getAveragePrice2(){
    Map<Integer,Product> product = DataStore.getInstance().getProDir().getProductDir();
    List<Product> productList = new ArrayList<Product>(product.values());
   
    Map<Integer,Order> order = DataStore.getInstance().getOrDir().getOrderDir();
    List<Order> orderList = new ArrayList<Order>(order.values());
    
    Map<Integer, Double> averagePrice = new HashMap<>();
    Map<Integer, Integer> tarPrice = new HashMap<>();
    Map<Integer, Double> diff = new HashMap<>();
    Map<Integer, Double> error = new HashMap<>();
    Map<Integer, String> isModified = new HashMap<>();
    Map<Integer, Integer> moPrice = new HashMap<>();
    Map<Integer, String> listed = new HashMap<>();
    
    System.out.println("5.2)Modified Data----------------------------------------");
    for(int i =0; i<product.size(); i++) {
         Product tmpProduct = product.get(i);
         int revenue = 0;
         int quantity = 0;
         for(int j =0;j< orderList.size();j++){
            if(order.get(j).getItem().getProductId() == tmpProduct.getProductId()){
               revenue += order.get(j).getItem().getSalesPrice()* order.get(j).getItem().getQuantity();
               quantity +=order.get(j).getItem().getQuantity(); 
            }  
        }
         averagePrice.put(i,(double)revenue/quantity);
         tarPrice.put(i,tmpProduct.getTarPrice());
         diff.put(i,(tarPrice.get(i) - averagePrice.get(i)));
         error.put(i,(diff.get(i)/averagePrice.get(i)));
         listed.put(i, "No");
         
         if( error.put(i,(diff.get(i)/averagePrice.get(i)))>=0.05 ||error.put(i,(diff.get(i)/averagePrice.get(i)))<=-0.05){
             isModified.put(i, "Yes");
             int mp = (int)Math.round(averagePrice.get(i)*(1.03));
             moPrice.put(i, mp);
             //tarPrice.put(i, mp);
         }else isModified.put(i,"No");
         
     }
    System.out.println("Table1:average sale price lower than the target price ");
       int t=0;
       for(int i=0; i<product.size(); i++){
           if(listed.get(i)=="No"){
           int num=i;
           for(int n=i+1;n<product.size();n++){
         if(diff.get(num)<diff.get(n) && (listed.get(n)=="No"))
         num=n;
           }
           if(diff.get(num)<0 && t==0){
            System.out.println("Table2:average sale price higher than the target price "); 
            t=1;
           }
         System.out.println("Product ID: " + num + "| Average Price: " + averagePrice.get(num) + " | Target Price: " +tarPrice.get(num) + " | Difference: " + diff.get(num) +" | Error: "+ error.get(num)*100+"%"+" |isModified: " + isModified.get(num));   
           listed.put(num,"Yes");
       }
     }
    System.out.println("Modified Product Price--------------------");
    for(Integer k : moPrice.keySet()){
        System.out.println("Product ID: "+k+ "   Modified Price: "+moPrice.get(k)+ "   Error:  "+ (moPrice.get(k)-averagePrice.get(k) )/averagePrice.get(k));
    }
    }
    
    
    
    
    
}
