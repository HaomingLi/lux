 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.analytics.AnalysisHelper;
import com.assignment5.analytics.DataStore;
import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class GateWay {
    
    DataReader orderReader;
    DataReader productReader;
    DataReader orderReader2;
    DataReader productReader2;
    
    AnalysisHelper helper;
    
     public GateWay() throws IOException {
        DataGenerator generator = DataGenerator.getInstance();
        orderReader = new DataReader(generator.getOrderFilePath());
        productReader = new DataReader(generator.getProductCataloguePath());
        helper = new AnalysisHelper();
        
        orderReader2 = new DataReader("SalesData_Ori.csv");
        productReader2 = new DataReader("ProductCatalogue_Ori.csv");
        
    }
     
    
    public static void main(String args[]) throws IOException{
        
      /**  DataGenerator generator = DataGenerator.getInstance();
        
        //Below is the sample for how you can use reader. you might wanna 
        //delete it once you understood.
        
        DataReader orderReader = new DataReader(generator.getOrderFilePath());
        String[] orderRow;
        printRow(orderReader.getFileHeader());
        while((orderRow = orderReader.getNextRow()) != null){
            printRow(orderRow);
        }
        System.out.println("_____________________________________________________________");
        DataReader productReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow;
        printRow(productReader.getFileHeader());
        while((prodRow = productReader.getNextRow()) != null){
            printRow(prodRow);
        }**/
        
        GateWay inst = new GateWay();
        inst.readData();
        DataStore.getNewDataStore();
        inst.readOriginData();
    }
    
    public static void printRow(String[] row){
        for (String row1 : row) {
            System.out.print(row1 + ", ");
        }
        System.out.println("");
    }
    
    private void readData() throws IOException{
        String[] orderRow;
        while((orderRow = orderReader.getNextRow()) != null){
            //printRow(orderRow);
            generateCustomer(orderRow);
            generateSalesperson(orderRow);
            generateOrder(orderRow);
           
        }
        
        String[] prodRow;
        
        while((prodRow = productReader.getNextRow()) != null){
            //printRow(prodRow);
            
            generateProduct(prodRow);
            
        }
        runAnalysis();
    }
    
    private void readOriginData() throws IOException{
        String[] orderRow;
        while((orderRow = orderReader2.getNextRow()) != null){
            //printRow(orderRow);
            generateCustomer(orderRow);
            generateSalesperson(orderRow);
            generateOrder(orderRow);
           
        }
        
        String[] prodRow;
        
        while((prodRow = productReader2.getNextRow()) != null){
            //printRow(prodRow);
            
            generateProduct(prodRow);
            
        }
        helper.getAveragePrice();
        helper.getAveragePrice2();
    }
    
    private void generateOrder(String[] orderRow){
        int orderId = Integer.parseInt(orderRow[0]);
        int itemId = Integer.parseInt(orderRow[1]);
        int proId = Integer.parseInt(orderRow[2]);
        int quantity = Integer.parseInt(orderRow[3]);
        int salesId = Integer.parseInt(orderRow[4]);
        int custId = Integer.parseInt(orderRow[5]);
        int price = Integer.parseInt(orderRow[6]);
        
        Item item = new Item(proId, price, quantity);
        
        Order order = new Order(orderId, salesId, custId, item);
        
        DataStore.getInstance().getOrDir().addOrder(order);
        
        Map<Integer, Customer> customers = DataStore.getInstance().getCustDir().getCustomerDir();
        Map<Integer, SalesPerson> salespersons = DataStore.getInstance().getSalesDir().getSalesPersonList();
        
        if(customers.containsKey(custId)){
             customers.get(custId).getOrderList().add(order);
             //System.out.println(customers.get(custId));
        }
       
        if(salespersons.containsKey(salesId)){
            salespersons.get(salesId).getOrderList().add(order);
           // System.out.println(salespersons.get(salesId));
        }
    }
    
    private void generateCustomer(String[] orderRow){
        int custId = Integer.parseInt(orderRow[5]);
        Customer c = new Customer(custId);
        DataStore.getInstance().getCustDir().addCustomer(c);
    }
    
    private void generateSalesperson(String[] orderRow){
       int salesId = Integer.parseInt(orderRow[4]);
       SalesPerson s = new SalesPerson(salesId);
       DataStore.getInstance().getSalesDir().addSalesPerson(s);
    }
    
    private void generateProduct(String[] prodRow){
        int proId = Integer.parseInt(prodRow[0]);
        int minPrice = Integer.parseInt(prodRow[1]);
        int maxPrice = Integer.parseInt(prodRow[2]);
        int tarPrice = Integer.parseInt(prodRow[3]);
        
        Product p = new Product(proId, minPrice, maxPrice, tarPrice);
        DataStore.getInstance().getProDir().addProduct(p);
    }
    
    
        
    private void runAnalysis(){
        helper.getTopThreeBestNegotiatedProd();
        helper.getThreeBestCustomers();
        helper.getThreeBestSalesPerson();
        helper.getTotalRevenue();
        helper.getAveragePrice();
        helper.getAveragePrice2();
    }
}
