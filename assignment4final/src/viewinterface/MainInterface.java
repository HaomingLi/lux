package viewinterface;

import viewinterface.agency.TravelManageJpanel;
import viewinterface.customer.CustomerInformationJpanel;
import viewinterface.airliner.AirlinerManageJpanel;
import java.awt.CardLayout;

import business.TravelAgency;

public class MainInterface extends javax.swing.JFrame {

    private TravelAgency agency;
    
    public MainInterface() {
        initComponents();
        this.agency = TravelAgency.getTravelInstance();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        splitPane = new javax.swing.JSplitPane();
        btnJpanel = new javax.swing.JPanel();
        travelAgencyBtn = new javax.swing.JButton();
        airlinerBtn = new javax.swing.JButton();
        customerBtn = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        functionJpanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        splitPane.setDividerLocation(160);

        btnJpanel.setBackground(new java.awt.Color(102, 204, 255));

        travelAgencyBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        travelAgencyBtn.setText("Travel Agency");
        travelAgencyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                travelAgencyBtnActionPerformed(evt);
            }
        });

        airlinerBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        airlinerBtn.setText("Airliner");
        airlinerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                airlinerBtnActionPerformed(evt);
            }
        });

        customerBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        customerBtn.setText("Customer");
        customerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerBtnActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Forte", 1, 24)); // NOI18N
        jLabel3.setText("LUX TRAVEL");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resource/LUX.png"))); // NOI18N

        javax.swing.GroupLayout btnJpanelLayout = new javax.swing.GroupLayout(btnJpanel);
        btnJpanel.setLayout(btnJpanelLayout);
        btnJpanelLayout.setHorizontalGroup(
            btnJpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnJpanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(btnJpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(travelAgencyBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(airlinerBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(customerBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(btnJpanelLayout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        btnJpanelLayout.setVerticalGroup(
            btnJpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(btnJpanelLayout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(travelAgencyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(airlinerBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(customerBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(jLabel3)
                .addContainerGap(72, Short.MAX_VALUE))
        );

        splitPane.setLeftComponent(btnJpanel);

        functionJpanel.setLayout(new java.awt.CardLayout());

        jLabel2.setBackground(new java.awt.Color(102, 204, 255));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resource/MJF2.jpg"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 603, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE)
        );

        functionJpanel.add(jPanel1, "card2");

        splitPane.setRightComponent(functionJpanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(splitPane)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void travelAgencyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_travelAgencyBtnActionPerformed
        functionJpanel.add(new TravelManageJpanel(functionJpanel, agency, agency.getCustomerMap()));
        CardLayout cLayout = (CardLayout) functionJpanel.getLayout();
        cLayout.next(functionJpanel);
    }//GEN-LAST:event_travelAgencyBtnActionPerformed

    private void airlinerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_airlinerBtnActionPerformed
        functionJpanel.add(new AirlinerManageJpanel(functionJpanel, agency));
        CardLayout cLayout = (CardLayout) functionJpanel.getLayout();
        cLayout.next(functionJpanel);
    }//GEN-LAST:event_airlinerBtnActionPerformed

    private void customerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerBtnActionPerformed
        functionJpanel.add(new CustomerInformationJpanel(functionJpanel, agency.getCustomerMap()));
        CardLayout cLayout = (CardLayout) functionJpanel.getLayout();
        cLayout.next(functionJpanel);
    }//GEN-LAST:event_customerBtnActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainInterface().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton airlinerBtn;
    private javax.swing.JPanel btnJpanel;
    private javax.swing.JButton customerBtn;
    private javax.swing.JPanel functionJpanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JButton travelAgencyBtn;
    // End of variables declaration//GEN-END:variables
}
