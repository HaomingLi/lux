package viewinterface.agency;

import viewinterface.customer.CustomerInformationJpanel;
import viewinterface.airliner.AirlinerInformation;
import java.awt.CardLayout;
import java.util.Map;
import javax.swing.JPanel;

import business.TravelAgency;

public class TravelManageJpanel extends javax.swing.JPanel {

    private JPanel funcJpanel;
    private TravelAgency agency;
    private Map customerMap;
    
    public TravelManageJpanel(JPanel funcJpanel, TravelAgency agency, Map customerMap) {
        initComponents();
        this.funcJpanel = funcJpanel;
        this.agency = agency;
        this.customerMap = customerMap;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        airlinerBtn = new javax.swing.JButton();
        customerBtn = new javax.swing.JButton();
        titleJabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(102, 204, 255));
        setForeground(new java.awt.Color(0, 153, 255));

        airlinerBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        airlinerBtn.setText("Airliner");
        airlinerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                airlinerBtnActionPerformed(evt);
            }
        });

        customerBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        customerBtn.setText("Customer");
        customerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerBtnActionPerformed(evt);
            }
        });

        titleJabel.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        titleJabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleJabel.setText("Travel Manage");

        jLabel2.setFont(new java.awt.Font("Old English Text MT", 0, 24)); // NOI18N
        jLabel2.setText("Join Lux Experience Luxary");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resource/Logo2.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(48, 48, 48)
                .addComponent(titleJabel, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(125, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(76, 76, 76))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(customerBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                            .addComponent(airlinerBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(150, 150, 150))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(titleJabel, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(airlinerBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(customerBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(130, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void airlinerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_airlinerBtnActionPerformed
        this.funcJpanel.add(new AirlinerInformation(this.funcJpanel, this.agency.getAirlinerMap(), this.customerMap));
        CardLayout cLayout = (CardLayout) this.funcJpanel.getLayout();
        cLayout.next(this.funcJpanel);
    }//GEN-LAST:event_airlinerBtnActionPerformed

    private void customerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerBtnActionPerformed
        this.funcJpanel.add(new CustomerInformationJpanel(this.funcJpanel, this.agency.getCustomerMap()));
        CardLayout cLayout = (CardLayout) this.funcJpanel.getLayout();
        cLayout.next(this.funcJpanel);
    }//GEN-LAST:event_customerBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton airlinerBtn;
    private javax.swing.JButton customerBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel titleJabel;
    // End of variables declaration//GEN-END:variables
}
