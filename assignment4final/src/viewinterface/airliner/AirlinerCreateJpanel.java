package viewinterface.airliner;

import java.awt.CardLayout;
import javax.swing.JPanel;
import java.util.Map;

import business.TravelAgency;
import business.Airliner;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class AirlinerCreateJpanel extends javax.swing.JPanel {

    private JPanel funcJpanel;
    private TravelAgency agency;

    public AirlinerCreateJpanel(JPanel funcJpanel, TravelAgency agency) {
        initComponents();
        this.funcJpanel = funcJpanel;
        this.agency = agency;
        flushTable();
    }

    private void flushTable() {
        DefaultTableModel dTable = (DefaultTableModel) airlinerTable.getModel();
        dTable.setRowCount(0);

        for (Map.Entry<Integer, Airliner> i : this.agency.getAirlinerMap().entrySet()) {
            Object row[] = new Object[4];

            Airliner airliner = i.getValue();
            row[0] = airliner;
            row[1] = airliner.getAirlinerName();
            row[2] = airliner.getAirlinerMap().size();
            row[3] = airliner.getUpdatetime();
            dTable.addRow(row);
        }
    }
    
    private boolean checkValid(String airlinerName){
        if (airlinerName == null || airlinerName.isEmpty()){
            JOptionPane.showMessageDialog(null, "Please input the airlinerName", "CREATE", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        backBtn = new javax.swing.JButton();
        createBtn = new javax.swing.JButton();
        viewAirplaneBtn = new javax.swing.JButton();
        titleJlael = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        airlinerTable = new javax.swing.JTable();
        airlinerJlabel = new javax.swing.JLabel();
        airlinerJtext = new javax.swing.JTextField();

        setBackground(new java.awt.Color(102, 204, 255));
        setForeground(new java.awt.Color(0, 153, 255));

        backBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        backBtn.setText("Back");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        createBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        createBtn.setText("Create");
        createBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createBtnActionPerformed(evt);
            }
        });

        viewAirplaneBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        viewAirplaneBtn.setText("View Airplane");
        viewAirplaneBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewAirplaneBtnActionPerformed(evt);
            }
        });

        titleJlael.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        titleJlael.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleJlael.setText("Airliner Create");

        airlinerTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "AirlinerID", "AirlinerName", "Airplane Num", "LastUpdateTime"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(airlinerTable);

        airlinerJlabel.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        airlinerJlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        airlinerJlabel.setText("Airliner Name:");

        airlinerJtext.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        airlinerJtext.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 57, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(airlinerJlabel)
                        .addGap(61, 61, 61)
                        .addComponent(airlinerJtext, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(75, 75, 75))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(viewAirplaneBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(146, 146, 146)
                        .addComponent(titleJlael, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(backBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(179, 179, 179)
                        .addComponent(createBtn)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(titleJlael, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(airlinerJlabel)
                    .addComponent(airlinerJtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addComponent(createBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(viewAirplaneBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        funcJpanel.remove(this);
        CardLayout cLayout = (CardLayout) funcJpanel.getLayout();
        cLayout.previous(funcJpanel);
    }//GEN-LAST:event_backBtnActionPerformed

    private void createBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createBtnActionPerformed
        String airlinerName = airlinerJtext.getText();
        if (!checkValid(airlinerName))
            return;
        
        Airliner airliner = this.agency.addAirliner(airlinerName);
        airliner.setAirlinerName(airlinerName);

        flushTable();
        JOptionPane.showMessageDialog(this, "Create Airliner success.", "CREATE", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_createBtnActionPerformed

    private void viewAirplaneBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewAirplaneBtnActionPerformed
        int selectedRow = airlinerTable.getSelectedRow(), col = 0;
        if (selectedRow < 0 ){
            JOptionPane.showMessageDialog(this, "Please select at least a row.", "CREATE", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        Airliner airliner = (Airliner) airlinerTable.getValueAt(selectedRow, col);
        this.funcJpanel.add(new AirplaneCreateJpanel(funcJpanel, airliner));
        CardLayout cLayout = (CardLayout) this.funcJpanel.getLayout();
        cLayout.next(this.funcJpanel);
    }//GEN-LAST:event_viewAirplaneBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel airlinerJlabel;
    private javax.swing.JTextField airlinerJtext;
    private javax.swing.JTable airlinerTable;
    private javax.swing.JButton backBtn;
    private javax.swing.JButton createBtn;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel titleJlael;
    private javax.swing.JButton viewAirplaneBtn;
    // End of variables declaration//GEN-END:variables
}
