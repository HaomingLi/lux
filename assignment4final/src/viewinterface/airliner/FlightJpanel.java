package viewinterface.airliner;

import java.awt.CardLayout;
import javax.swing.JPanel;

import business.Flight;

public class FlightJpanel extends javax.swing.JPanel {
    
    private JPanel funcJpanel;
    private Flight flight;

    public FlightJpanel(JPanel funcJpanel, Flight flight) {
        initComponents();
        this.funcJpanel = funcJpanel;
        this.flight = flight;
        initData();
    }

    
    private void initData(){
        departureJtext.setText(this.flight.getDeparture());
        arrivalJtext.setText(this.flight.getArrival());
        departureTimeJtext.setText(this.flight.getDepartureTime().toString());
        arrivalTimeJtext.setText(this.flight.getArrivalTime().toString());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        arrivalTimeJlabel = new javax.swing.JLabel();
        arrivalTimeJtext = new javax.swing.JTextField();
        titleJlael = new javax.swing.JLabel();
        backBtn = new javax.swing.JButton();
        departureTimeJtext = new javax.swing.JTextField();
        departureJlabel = new javax.swing.JLabel();
        departureJtext = new javax.swing.JTextField();
        arrivalJlabel = new javax.swing.JLabel();
        arrivalJtext = new javax.swing.JTextField();
        departureTimeJlabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(102, 204, 255));

        arrivalTimeJlabel.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        arrivalTimeJlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        arrivalTimeJlabel.setText("Arrival Time:");

        arrivalTimeJtext.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        arrivalTimeJtext.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        arrivalTimeJtext.setEnabled(false);

        titleJlael.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        titleJlael.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleJlael.setText("Flight Information");

        backBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        backBtn.setText("Back");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        departureTimeJtext.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        departureTimeJtext.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        departureTimeJtext.setEnabled(false);

        departureJlabel.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        departureJlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        departureJlabel.setText("Departure:");

        departureJtext.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        departureJtext.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        departureJtext.setEnabled(false);

        arrivalJlabel.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        arrivalJlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        arrivalJlabel.setText("Arrival:");

        arrivalJtext.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        arrivalJtext.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        arrivalJtext.setEnabled(false);

        departureTimeJlabel.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        departureTimeJlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        departureTimeJlabel.setText("Departure Time:");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resource/Logo2.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(78, 78, 78)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(departureJlabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(arrivalTimeJlabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(departureTimeJlabel)
                            .addComponent(arrivalJlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(2, 2, 2)))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(departureTimeJtext, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(arrivalTimeJtext, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(arrivalJtext, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(departureJtext, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 108, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(titleJlael, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(91, 91, 91)
                        .addComponent(backBtn)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(titleJlael, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(departureJlabel)
                    .addComponent(departureJtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(arrivalJlabel)
                    .addComponent(arrivalJtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(departureTimeJlabel)
                    .addComponent(departureTimeJtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(arrivalTimeJlabel)
                    .addComponent(arrivalTimeJtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(48, 48, 48)
                .addComponent(backBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(75, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        funcJpanel.remove(this);
        CardLayout cLayout = (CardLayout)funcJpanel.getLayout();
        cLayout.previous(funcJpanel);
    }//GEN-LAST:event_backBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel arrivalJlabel;
    private javax.swing.JTextField arrivalJtext;
    private javax.swing.JLabel arrivalTimeJlabel;
    private javax.swing.JTextField arrivalTimeJtext;
    private javax.swing.JButton backBtn;
    private javax.swing.JLabel departureJlabel;
    private javax.swing.JTextField departureJtext;
    private javax.swing.JLabel departureTimeJlabel;
    private javax.swing.JTextField departureTimeJtext;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel titleJlael;
    // End of variables declaration//GEN-END:variables
}
