package viewinterface.airliner;


import java.awt.CardLayout;
import javax.swing.JPanel;

import business.Airplane;
import business.Flight;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FlightCreateJpanel extends javax.swing.JPanel {

    private JPanel funcJpanel;
    private Airplane airplane;
    
    public FlightCreateJpanel(JPanel funcJpanel, Airplane airplane) {
        initComponents();
        this.funcJpanel = funcJpanel;
        this.airplane = airplane;
        flushTable();
    }

    private void flushTable() {
        DefaultTableModel dTable = (DefaultTableModel) flightTable.getModel();
        dTable.setRowCount(0);

        for (Map.Entry<Integer, Flight> i : this.airplane.getFlightMap().entrySet()) {
            Object row[] = new Object[6];

            Flight flight = i.getValue();
            row[0] = flight;
            row[1] = flight.getDeparture();
            row[2] = flight.getArrival();
            row[3] = flight.getDepartureTime();
            row[4] = flight.getArrivalTime();
            row[5] = getLastTimeString(flight.getLastTime()/1000);
            dTable.addRow(row);
        }
    }
    
    private boolean checkValid(String departure, String arrival, String departureTime, String arrivalTime){
        if (departure == null || departure.isEmpty()){
            JOptionPane.showMessageDialog(null, "Please input the departure", "CREATE", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (arrival == null || arrival.isEmpty()){
            JOptionPane.showMessageDialog(null, "Please input the arrival", "CREATE", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (departureTime == null || departureTime.isEmpty()){
            JOptionPane.showMessageDialog(null, "Please input the departureTime", "CREATE", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (!isDateString(departureTime)){
            JOptionPane.showMessageDialog(null, "Please input the correct departureTime:yyyy-mm-dd HH:MM:SS", "CREATE", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (arrivalTime == null || arrivalTime.isEmpty()){
            JOptionPane.showMessageDialog(null, "Please input the arrivalTime", "CREATE", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (!isDateString(arrivalTime)){
            JOptionPane.showMessageDialog(null, "Please input the correct arrivalTime:yyyy-mm-dd HH:MM:SS", "CREATE", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
    
    private boolean isDateString(String str){
        String reg = "^^((([0-9]{2})(0[48]|[2468][048]|[13579][26]))|((0[48]|[2468][048]|[13579][26])00)-02-29)"
                + "|([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-"
                + "(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))) "
                + "(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d$" ;
        return str.matches(reg);
    }
    
    private String getLastTimeString(int totalTime){
        String totalTimeStr = totalTime / 86400 + "d" + (totalTime / 3600) % 24 + "h" + (totalTime % 3600) / 60 + "min" + (totalTime % 3600) % 60 + "s";
        return totalTimeStr;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleJlael = new javax.swing.JLabel();
        backBtn = new javax.swing.JButton();
        departureTimeJtext = new javax.swing.JTextField();
        departureJlabel = new javax.swing.JLabel();
        departureJtext = new javax.swing.JTextField();
        arrivalJlabel = new javax.swing.JLabel();
        arrivalJtext = new javax.swing.JTextField();
        departureTimeJlabel = new javax.swing.JLabel();
        arrivalTimeJlabel = new javax.swing.JLabel();
        arrivalTimeJtext = new javax.swing.JTextField();
        createBtn = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        flightTable = new javax.swing.JTable();

        setBackground(new java.awt.Color(102, 204, 255));

        titleJlael.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        titleJlael.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleJlael.setText("Flight Create");

        backBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        backBtn.setText("Back");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        departureTimeJtext.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        departureTimeJtext.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        departureJlabel.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        departureJlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        departureJlabel.setText("Departure:");

        departureJtext.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        departureJtext.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        arrivalJlabel.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        arrivalJlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        arrivalJlabel.setText("Arrival:");

        arrivalJtext.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        arrivalJtext.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        departureTimeJlabel.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        departureTimeJlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        departureTimeJlabel.setText("Departure Time:");

        arrivalTimeJlabel.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        arrivalTimeJlabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        arrivalTimeJlabel.setText("Arrival Time:");

        arrivalTimeJtext.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        arrivalTimeJtext.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        createBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        createBtn.setText("Create");
        createBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createBtnActionPerformed(evt);
            }
        });

        flightTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "FlightID", "Departure", "Arrival", "DepartureTime", "ArrivalTime", "last"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        flightTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(flightTable);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(116, 116, 116)
                .addComponent(arrivalTimeJlabel, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                .addGap(32, 32, 32)
                .addComponent(arrivalTimeJtext, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(209, 209, 209))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(backBtn)
                        .addGap(52, 52, 52)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(arrivalJlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(departureJlabel, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(departureTimeJlabel))
                        .addGap(53, 53, 53)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(arrivalJtext, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(departureJtext, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(departureTimeJtext, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(144, 144, 144)
                        .addComponent(titleJlael, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(107, 107, 107)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 368, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(243, 243, 243)
                .addComponent(createBtn)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleJlael, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(departureTimeJlabel)
                            .addComponent(departureTimeJtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(arrivalTimeJlabel)
                            .addComponent(arrivalTimeJtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(departureJlabel)
                            .addComponent(departureJtext, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(arrivalJlabel)
                            .addComponent(arrivalJtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(backBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)))
                .addComponent(createBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        funcJpanel.remove(this);
        CardLayout cLayout = (CardLayout)funcJpanel.getLayout();
        cLayout.previous(funcJpanel);
    }//GEN-LAST:event_backBtnActionPerformed

    private void createBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createBtnActionPerformed
        String departure = departureJtext.getText();
        String arrival = arrivalJtext.getText();
        String departureTime = departureTimeJtext.getText();
        String arrivalTime = arrivalTimeJtext.getText();
        if (!checkValid(departure, arrival, departureTime, arrivalTime))
            return;
        
        Flight flight = this.airplane.addFlight();
        flight.setDeparture(departure);
        flight.setArrival(arrival);
        flight.setSeatTotal(this.airplane.getSeatCapacity());
        flight.generateSeat();

        flushTable();
        JOptionPane.showMessageDialog(this, "Create Flight success.", "CREATE", JOptionPane.INFORMATION_MESSAGE);
        
    }//GEN-LAST:event_createBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel arrivalJlabel;
    private javax.swing.JTextField arrivalJtext;
    private javax.swing.JLabel arrivalTimeJlabel;
    private javax.swing.JTextField arrivalTimeJtext;
    private javax.swing.JButton backBtn;
    private javax.swing.JButton createBtn;
    private javax.swing.JLabel departureJlabel;
    private javax.swing.JTextField departureJtext;
    private javax.swing.JLabel departureTimeJlabel;
    private javax.swing.JTextField departureTimeJtext;
    private javax.swing.JTable flightTable;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel titleJlael;
    // End of variables declaration//GEN-END:variables
}
