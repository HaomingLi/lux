package viewinterface.airliner;

import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.JPanel;

import business.Airliner;
import business.Airplane;
import business.Flight;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
import java.util.Date;
import tools.ConstantDict;

public class AirlinerSearchJpanel extends javax.swing.JPanel {

    private JPanel funcJpanel;
    private Map<Integer, Airliner> airlinerMap;
    private ArrayList<Flight> flightList = new ArrayList();
    private Map customerMap;

    public AirlinerSearchJpanel(JPanel funcJpanel, Map airlinerMap, Map customerMap) {
        initComponents();
        this.funcJpanel = funcJpanel;
        this.airlinerMap = airlinerMap;
        this.customerMap = customerMap;
        initData();
    }

    private void initData() {
        flushTable();
        initBox();
    }

    private void flushTable() {
        DefaultTableModel dTable = (DefaultTableModel) airlinerTable.getModel();
        dTable.setRowCount(0);

        for (Map.Entry<Integer, Airliner> i : this.airlinerMap.entrySet()) {
            Object row[] = new Object[8];

            Airliner airliner = i.getValue();
            for (Map.Entry<Integer, Airplane> j : airliner.getAirlinerMap().entrySet()) {
                Airplane airplane = j.getValue();
                for (Map.Entry<Integer, Flight> element : airplane.getFlightMap().entrySet()) {
                    Flight flight = element.getValue();
                    row[0] = flight;
                    row[1] = airplane.getCompany();
                    row[2] = airplane.getSeatCapacity();
                    row[3] = flight.getDeparture();
                    row[4] = flight.getArrival();
                    row[5] = getTimeString(flight.getDepartureTime());
                    row[6] = getTimeString(flight.getArrivalTime());
                    row[7] = getLastTimeString(flight.getLastTime() / 1000);
                    dTable.addRow(row);
                }
            }
        }
    }
    
    private String getTimeString(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String now = dateFormat.format(date); 
        String nowHour = now.substring(11, 13);
        return ConstantDict.planeHourMap.get(Integer.parseInt(nowHour));
    }

    private void initBox() {
        Set<String> departureSet = new HashSet();
        Set<String> arrivalSet = new HashSet();
        for (Map.Entry<Integer, Airliner> airliners : this.airlinerMap.entrySet()) {
            Airliner airliner = airliners.getValue();
            for (Map.Entry<Integer, Airplane> airplanes : airliner.getAirlinerMap().entrySet()) {
                Airplane airplane = airplanes.getValue();
                for (Map.Entry<Integer, Flight> flights : airplane.getFlightMap().entrySet()) {
                    Flight flight = flights.getValue();
                    this.flightList.add(flight);
                    departureSet.add(flight.getDeparture());
                    arrivalSet.add(flight.getArrival());
                }
            }
        }

        departureBox.removeAllItems();
        for (String departure : departureSet) {
            departureBox.addItem(departure);
        }

        arrivalBox.removeAllItems();
        for (String arrival : arrivalSet) {
            arrivalBox.addItem(arrival);
        }

    }

    private ArrayList getSearchResult() {
        String departure = (String) departureBox.getSelectedItem();
        String arrival = (String) arrivalBox.getSelectedItem();

        ArrayList<Flight> searchList = new ArrayList();
        for (Map.Entry<Integer, Airliner> airliners : this.airlinerMap.entrySet()) {
            Airliner airliner = airliners.getValue();
            for (Map.Entry<Integer, Airplane> airplanes : airliner.getAirlinerMap().entrySet()) {
                Airplane airplane = airplanes.getValue();
                for (Map.Entry<Integer, Flight> flights : airplane.getFlightMap().entrySet()) {
                    Flight flight = flights.getValue();
                    if (flight.getDeparture().equalsIgnoreCase(departure) && flight.getArrival().equalsIgnoreCase(arrival)) {
                        searchList.add(flight);
                    }
                }
            }
        }
        return searchList;
    }

    private String getLastTimeString(int totalTime) {
        String totalTimeStr = totalTime / 86400 + "d" + (totalTime / 3600) % 24 + "h" + (totalTime % 3600) / 60 + "min" + (totalTime % 3600) % 60 + "s";
        return totalTimeStr;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleJlael = new javax.swing.JLabel();
        viewBtn = new javax.swing.JButton();
        deleteBtn = new javax.swing.JButton();
        backBtn = new javax.swing.JButton();
        departureBox = new javax.swing.JComboBox<>();
        arrivalBox = new javax.swing.JComboBox<>();
        searchBtn = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        airlinerTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(102, 204, 255));
        setForeground(new java.awt.Color(0, 153, 255));

        titleJlael.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        titleJlael.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        titleJlael.setText("Airliner Search");

        viewBtn.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        viewBtn.setText("View");
        viewBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewBtnActionPerformed(evt);
            }
        });

        deleteBtn.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        deleteBtn.setText("Delete");
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });

        backBtn.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        backBtn.setText("Back");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        departureBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        arrivalBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        searchBtn.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        searchBtn.setText("Search");
        searchBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBtnActionPerformed(evt);
            }
        });

        airlinerTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "FlightID", "Company", "SeatCapacity", "Departure", "Arrival", "DepartureTime", "ArrivalTime", "Last"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        airlinerTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(airlinerTable);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel1.setText("Departure:");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 2, 14)); // NOI18N
        jLabel2.setText("Arrival:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(viewBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(deleteBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(arrivalBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(departureBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(127, 127, 127)
                                .addComponent(titleJlael, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 139, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(backBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(searchBtn)
                        .addGap(92, 92, 92)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleJlael, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(departureBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(arrivalBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(backBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(searchBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(viewBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(50, 50, 50)
                        .addComponent(deleteBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        funcJpanel.remove(this);
        CardLayout cLayout = (CardLayout) funcJpanel.getLayout();
        cLayout.previous(funcJpanel);
    }//GEN-LAST:event_backBtnActionPerformed

    private void searchBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBtnActionPerformed
        ArrayList flightList = getSearchResult();
        if (flightList.isEmpty()) {
            return;
        }
        this.funcJpanel.add(new FlightSearchJpanel(this.funcJpanel, flightList, this.customerMap));
        CardLayout cLayout = (CardLayout) this.funcJpanel.getLayout();
        cLayout.next(funcJpanel);
    }//GEN-LAST:event_searchBtnActionPerformed

    private void viewBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewBtnActionPerformed
        int selectedRow = airlinerTable.getSelectedRow(), col = 0;
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Please select at least a row.", "INFORMATION", JOptionPane.ERROR_MESSAGE);
            return;
        }

        Flight flight = (Flight) airlinerTable.getValueAt(selectedRow, col);
        this.funcJpanel.add(new FlightJpanel(this.funcJpanel, flight));
        CardLayout cLayout = (CardLayout) this.funcJpanel.getLayout();
        cLayout.next(funcJpanel);
    }//GEN-LAST:event_viewBtnActionPerformed

    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtnActionPerformed
        int selectedRow = airlinerTable.getSelectedRow(), col = 0;
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Please select at least a row.", "CREATE", JOptionPane.ERROR_MESSAGE);
            return;
        }

        Flight flight = (Flight) airlinerTable.getValueAt(selectedRow, col);
        for (Map.Entry<Integer, Airliner> airliners : this.airlinerMap.entrySet()) {
            Airliner airliner = airliners.getValue();
            for (Map.Entry<Integer, Airplane> airplanes : airliner.getAirlinerMap().entrySet()) {
                Airplane airplane = airplanes.getValue();
                airplane.getFlightMap().remove(flight.getFlightID());
            }
        }
        flushTable();
        JOptionPane.showMessageDialog(this, "Delete success", "DELETE", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_deleteBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable airlinerTable;
    private javax.swing.JComboBox<String> arrivalBox;
    private javax.swing.JButton backBtn;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JComboBox<String> departureBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton searchBtn;
    private javax.swing.JLabel titleJlael;
    private javax.swing.JButton viewBtn;
    // End of variables declaration//GEN-END:variables
}
